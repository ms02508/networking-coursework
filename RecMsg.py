class RecMsg:
    """pass incoming message into this class to break it into it's components"""

    def __init__(self, message, encrypted):
        # split the package into its individual components
        self.packID = message[0:2]
        self.totalPackSize = message[2:4]
        self.checkSum = message[4:6]
        self.emptyBytes = message[6:10]
        self.data = message[10:]
        self.isEncrypted = encrypted
        if self.testForImagePacket():
            self.isImgPack = True
        else:
            self.isImgPack = False

        # self.plainData = self.decryptData()

    def getPackID(self):
        return self.packID

    def getTotalPackSize(self):
        return self.totalPackSize

    def getCheckSum(self):
        return self.checkSum

    def getData(self):
        return self.data

    def getIsImgPack(self):
        return self.isImgPack

    def decryptData(self, override=False):
        numPackets = int.from_bytes(self.totalPackSize, "big")
        b_msg = self.data
        # print(b_msg)

        if not self.isImgPack:
            if self.isEncrypted or override:
                key = (69 - numPackets) * 2
                ciphertext = b_msg.decode()
                decrypted = ""
                for c in ciphertext:
                    if c.isupper():
                        c_index = ord(c) - ord('A')
                        # shift the current character to left by key positions to get its original position
                        c_og_pos = (c_index - key) % 26 + ord('A')
                        c_og = chr(c_og_pos)
                        decrypted += c_og
                    elif c.islower():
                        c_index = ord(c) - ord('a')
                        c_og_pos = (c_index - key) % 26 + ord('a')
                        c_og = chr(c_og_pos)
                        decrypted += c_og
                    elif c.isdigit():
                        # if it's a number,shift its actual value
                        c_og = (int(c) - key) % 10
                        decrypted += str(c_og)
                    else:
                        # if its neither alphabetical nor a number, just leave it like that
                        decrypted += c

            else:
                decrypted = b_msg.decode()
        else:
            decrypted = "cannot decode image data"

        return decrypted

    def testForSendReq(self):
        try:
            if (
                    self.data == b"011000100111010101101110011001000110000100001010"
                    or
                    self.decryptData(True) == "011000100111010101101110011001000110000100001010"
            ):
                return True
            else:
                return False
        except Exception as e:
            return False

    def testForAck(self):
        # add extra case incase the ack message was encrypted
        if self.data == b"ACK" or (self.decryptData(True) == "ACK"):
            return True
        else:
            return False

    def testForResend(self):
        msg = bytes("011100000110111101100111", "utf-8")
        if self.data == msg or self.decryptData(True) == msg:
            return True
        else:
            return False

    def testForImgData(self):
        # print("testing for image data")
        msg = b"image_data"
        if self.data[0:10] == msg:
            print("is img data")
            return True
        else:
            return False

    def testForImagePacket(self):
        if self.emptyBytes == b"_img":
            # print("IMAGE")
            # print("&"*10)
            return True
        else:
            return False
