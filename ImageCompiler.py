import cv2
import numpy as np
import tkinter as tk
from math import sqrt


class ImageCompiler:
    def __init__(self, w, h, c):
        self.width = w
        self.height = h
        self.channels = c
        print("w", w, "h", h, "c", c)
        self.imagePacks = []
        self.imageBytes = b""

    def addImgPack(self, imgPack):
        self.imagePacks.append(imgPack)
        # print("added image")

    def emptyPackList(self):
        if len(self.imagePacks) == 0:
            return True
        else:
            return False

    def orderData(self):
        print("ordering data")
        validated_arr = []
        for i in range(len(self.imagePacks)):
            for pack in self.imagePacks:
                if int.from_bytes(pack.getPackID(), "big") == i + 1:
                    validated_arr.append(pack)
                    break
        self.imagePacks = validated_arr

    def getImgData(self):
        for packet in self.imagePacks:
            # get image data and append it to imageBytes
            self.imageBytes += packet.getData()

    def decodeImage(self):
        # print("decoding image")
        # print(self.imageBytes)
        decoded = np.fromstring(self.imageBytes, dtype=np.uint8)
        # We have to convert it into (270, 480,3) in order to see as an image
        decoded = decoded.reshape((self.height, self.width, self.channels))
        return decoded

    def displayImg(self):
        # self.orderData()
        self.getImgData()
        decoded_img = self.decodeImage()
        print("cv2 imshow")

        area = 0.25
        h, w = decoded_img.shape[:2]
        root = tk.Tk()
        screen_h = root.winfo_screenheight()
        screen_w = root.winfo_screenwidth()
        vector = sqrt(area)
        window_h = screen_h * vector
        window_w = screen_w * vector

        if h > window_h or w > window_w:
            if h / window_h >= w / window_w:
                multiplier = window_h / h
            else:
                multiplier = window_w / w
            decoded_img = cv2.resize(decoded_img, (0, 0), fx=multiplier, fy=multiplier)

        cv2.imshow('received_img', decoded_img)
        # print(decoded.shape)
        cv2.waitKey(0)
        cv2.destroyAllWindows()


class EmptyCompiler:
    @staticmethod
    def emptyPackList():
        return True
