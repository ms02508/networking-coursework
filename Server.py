# Matthew Samm
# COM2022 - Computer Networking

# imports
from socket import *

from ImageCompiler import ImageCompiler, EmptyCompiler
from RecMsg import RecMsg


class Server:
    """Server Class to handle receiving the messages"""

    def __init__(self, portNum, doDecrypt=False):
        self.serverSocket = socket(AF_INET, SOCK_DGRAM)
        self.serverSocket.bind(("", portNum))
        self.portNum = portNum
        self.openConn = False
        self.doDecrypt = doDecrypt

    def setDecrypt(self, doDecrypt):
        self.doDecrypt = doDecrypt

    def getDecrypt(self):
        return self.doDecrypt

    def decodeMessageBytes(self, packets):
        message = ""
        b_message = b""
        # for i in new_packets:
        # print(i.decryptData(), end="")
        for p in packets:
            b_message += p.getData()

        # print(b_message.hex())
        for b in b_message:
            print(b, end=", ")
        print()
        message = b_message.decode("utf-8")

        return message

    def openCon(self):
        """open the socket to receive packets"""
        self.openConn = True
        message_packets = []
        printedPackets = False
        rec_image = EmptyCompiler()
        while self.openConn:
            # bytes value (256) == len(bytes(msg, "utf-8")
            message, address = self.serverSocket.recvfrom(256)

            # following code executed if a message is received
            if message is not None:
                # test code to validate a message was received
                # print(str(message))

                # create a recMsg object out of the rec message
                # allows for removal of the header
                # implement a function to test if it's the message request
                rec_message = RecMsg(message, encrypted=self.doDecrypt)

                if rec_message.testForSendReq():
                    # send ack package
                    print("received request")
                    self.sendAck(address)
                    print(address)
                else:
                    if rec_message.testForImgData():
                        self.sendAck(address)
                        print("img data received")
                        # export image details from packet

                        img_data = rec_message.getData()
                        enc_h = img_data[10:12]
                        enc_w = img_data[12:14]
                        enc_c = img_data[14:16]

                        height = int.from_bytes(enc_h, byteorder="big")
                        width = int.from_bytes(enc_w, byteorder="big")
                        channels = int.from_bytes(enc_c, byteorder="big")

                        rec_image = ImageCompiler(width, height, channels)

                        # message, address = self.serverSocket.recvfrom(256)
                        # rec_message = RecMsg(message, encrypted=self.doDecrypt)
                    # print("test request FAILED")
                    # test if received packet was part of a longer message

                    total_packs = int.from_bytes(rec_message.getTotalPackSize(), byteorder="big")

                    if total_packs > 1:
                        # message spans more than 1 packet
                        pack_id = int.from_bytes(rec_message.getPackID(), byteorder="big")
                        print("packet ID: ", pack_id, "total packets: ", total_packs)

                        if len(message_packets) == 0 and rec_image.emptyPackList():
                            if rec_message.getIsImgPack():
                                # print("first iamge pack")
                                rec_image.addImgPack(rec_message)
                                self.sendAck(address, silent=True)
                            else:
                                message_packets.append(rec_message)

                        else:
                            # test for final packet
                            if pack_id == total_packs:
                                # add the final packet to the array
                                print("final packet")

                                if rec_message.getIsImgPack():
                                    rec_image.addImgPack(rec_message)
                                    self.sendAck(address, silent=True)
                                    print("got whole image")

                                    rec_image.displayImg()

                                    printedPackets = False

                                else:
                                    message_packets.append(rec_message)

                                    # print(message_packets)
                                    # del message_packets[2]
                                    # print(message_packets)

                                    # validate packets
                                    validated, val_packets = self.verifyPacks(message_packets, address,
                                                                              checkNumPacks=True)
                                    if validated:
                                        new_packets = self.addNewPacks(val_packets, message_packets, total_packs)
                                        print(self.decodeMessageBytes(new_packets))
                                        # for i in new_packets:
                                        #     print(i.decryptData(), end="")

                                        print()
                                        message_packets = []
                                        val_packets = []
                                        printedPackets = False
                                        self.sendAck(address, silent=True)

                                    else:
                                        print("An Unexpected error occurred.")
                                        message_packets = []
                                        val_packets = []
                                        printedPackets = False

                            else:
                                if rec_message.getIsImgPack():
                                    rec_image.addImgPack(rec_message)
                                    self.sendAck(address, silent=True)
                                else:
                                    message_packets.append(rec_message)

                    else:
                        # message is all on one packet
                        message_packets.append(rec_message)

                        validated, val_packets = self.verifyPacks(message_packets, address)
                        if validated:
                            if not rec_message.getIsImgPack():
                                print(rec_message.decryptData())
                                printedPackets = False
                            # else:
                            #     rec_image.addImgPack(rec_message)
                            #     # image is all on one packet, so can then show image
                            #     rec_image.displayImg()
                            self.sendAck(address)
                            val_packets = []
                            message_packets = []

                        else:
                            print("An Unexpected error occurred.")
                            val_packets = []
                            message_packets = []

    def sendAck(self, address, silent=False):
        """send an acknowledgement to the given address"""
        if not silent:
            print("sending ack")

        msg = bytes("ACK", "utf-8")
        check = (3).to_bytes(2, "big")
        pack_id = (0).to_bytes(2, "big")
        total_pack = (1).to_bytes(2, "big")
        blank = (0).to_bytes(4, "big")
        header = pack_id + total_pack + check + blank

        pack = header + msg
        # b_pack = pack.encode("utf-8")

        self.serverSocket.sendto(pack, address)

    def addNewPacks(self, newPacks, packs, totalPacks):
        print("merging packets")
        total_packets_arr = []
        for i in range(totalPacks):
            print(i)
            for p in packs:
                if (int.from_bytes(p.getPackID(), "big")) == i:
                    total_packets_arr.append(p)

            for p in newPacks:
                if (int.from_bytes(p.getPackID(), "big")) == i:
                    total_packets_arr.append(p)

        print(total_packets_arr)
        return total_packets_arr

    def verifyPacks(self, packets, address, checkNumPacks=False):
        """check the packets have the correct checksum. And that there are no packets missing."""
        # empty list, store the ID of the packets that need to be re-sent
        req_packs = []

        # list to store the packets in once they have been received after the re-send request
        val_packs = []
        pack_ids = []

        validated = False

        # first, validate checksums
        for p in packets:
            check = int.from_bytes(p.getCheckSum(), byteorder="big")
            pack_id = int.from_bytes(p.getPackID(), byteorder="big")
            msg = p.getData()
            pack_ids.append(pack_id)

            if not self.checkChecksum(check, msg):
                # incorrect checksum, need to request packets to be re-sent
                print("incorrect checksum")
                req_packs.append(pack_id)

        # only validate number of packets for messages that required more than one
        if checkNumPacks:
            # validate total packets
            packet_totals = []
            for i in range(0, len(packets)):
                packet_totals.append(int.from_bytes(packets[i].getTotalPackSize() + 1, byteorder="big"))

            print("packet totals", packet_totals)

            if packet_totals.count(packet_totals[0]) == len(packet_totals):
                # print("all totals equal")
                # check that there are no packets missing
                if len(packets) != packet_totals[0]:
                    # missing a packet
                    print("missing a packet")

                    # get IDs of missing packets
                    for i in range(1, packet_totals[0] + 1):
                        if i not in pack_ids:
                            req_packs.append(i)

                    print("req_packets:", req_packs)
                    val_packs = self.requestPackets(req_packs, address)
                    if val_packs is not None:
                        validated = True

                elif len(packets) == packet_totals[0]:
                    print("got all packets")
                    validated = True
                    val_packs = packets

            else:
                # request all packets to be re-sent as cannot know how many there should be
                val_packs = self.requestPackets(req_packs, address)
                if val_packs is not None:
                    validated = True

        else:
            if len(req_packs) == 0:
                validated = True
                val_packs = packets

        return validated, val_packs

    def requestPackets(self, requested, address):
        """Handles communication to sender to request re-transmission of packets."""
        print("requesting packets")

        # compile packet
        msg = bytes("011100000110111101100111", "utf-8")
        check = (24).to_bytes(2, "big")
        pack_id = (0).to_bytes(2, "big")
        total_pack = (1).to_bytes(2, "big")
        blank = (0).to_bytes(4, "big")
        header = pack_id + total_pack + check + blank

        pack = header + msg

        # send packet
        self.serverSocket.sendto(pack, address)

        # wait for message
        # then test if it's ACK

        self.serverSocket.settimeout(1)
        new_packs = []

        try:
            data, address_serv = self.serverSocket.recvfrom(256)
        except timeout as inst:
            print("Rec ack timout, retransmit")
        else:
            # enters else when message received
            # test for ACK
            rec_msg = RecMsg(data, False)

            if rec_msg.testForAck():
                print("OK to send packet list")
                # send list of packets
                self.serverSocket.sendto(self.createReqPacket(requested), address)

                # listen for response
                print("listening")
                for i in requested:
                    try:
                        data, address = self.serverSocket.recvfrom(256)
                    except timeout as inst:
                        print("socket timeout")
                    else:
                        new_packet = RecMsg(data, encrypted=self.doDecrypt)
                        new_packs.append(new_packet)

            else:
                print("unexpected error. try again later")

        self.serverSocket.settimeout(None)
        print(new_packs)
        return new_packs

    def createReqPacket(self, requested):
        """compile the packet that contains the list of packets that need to be re-sent"""

        msg = ""
        for i in range(len(requested)):
            if i + 1 != len(requested):
                msg += str(requested[i]) + ","
            else:
                msg += str(requested[i])

        print(msg)
        b_msg = bytes(msg, "utf-8")

        # header
        check = (len(b_msg)).to_bytes(2, "big")
        p_id = (0).to_bytes(2, "big")
        total = (1).to_bytes(2, "big")
        blank = (0).to_bytes(4, "big")

        header = p_id + total + check + blank

        packet = header + b_msg

        return packet

    def checkChecksum(self, check, msg):
        """verify that the checksum in the header matches the calculated checksum"""
        # "create" checksum from the msg
        new_check = len(msg)
        # validate checksum in range 1-99
        new_check %= 100

        # compare new_check to check
        if new_check == check:
            cor_check = True
        else:
            cor_check = False

        return cor_check

    def closeCon(self):
        """close the socket"""
        self.openConn = False
        self.serverSocket.close()

    def changePort(self, newPort):
        # first clear the current socket
        self.closeCon()
        self.serverSocket = None

        self.portNum = newPort
        self.serverSocket = socket(AF_INET, SOCK_DGRAM)
        self.serverSocket.bind(("", newPort))

    def getPortNum(self):
        return self.portNum
