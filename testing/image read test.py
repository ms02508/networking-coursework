import cv2
import numpy as np
import tkinter as tk
from math import sqrt

# im = cv2.imread('test.jpg')
# with open('test.jpg', "rb") as f:
#     byte_im = f.read()
# # fd = open('test.jpg')
# # img_str = fd.read()
# # fd.close()
# for b in byte_im:
#     print(b)
# print(byte_im)
# print(len(byte_im))
#
#
# cv2.imshow('image', img_np)
# cv2.waitKey(5_000)
# cv2.destroyAllWindows()


def encodeImage(img):
    h, w, c = img.shape
    # resize inserted image
    data = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    data = cv2.resize(data, (w, h))

    # run a color convert:
    data = cv2.cvtColor(data, cv2.COLOR_BGR2RGB)
    return bytes(data), w, h, c  # encode Numpay to Bytes string


def decodeImage(data, width, height, channels):
    # Gives us 1d array

    decoded = np.fromstring(data, dtype=np.uint8)
    # We have to convert it into (270, 480,3) in order to see as an image
    decoded = decoded.reshape((height, width, channels))

    return decoded


# Load an color image

# imread works with absolute path and relative path

abs_path = "C:\\Users\\mcsam\\Pictures\\logo.png"
rel_path = "../UDP lab/DpKzIQ.jpg"
fez = "../fez2.jpeg"
image = cv2.imread(fez, 1)

img_code, w, h, c = encodeImage(image)  # Output: b'\xff\xd8\xff\xe0\x00\x10...';
img = decodeImage(img_code, w, h, c)  # Output: normal array

# area = 0.25
#
# h, w = img.shape[:2]
# root = tk.Tk()
# screen_h = root.winfo_screenheight()
# screen_w = root.winfo_screenwidth()
# vector = sqrt(area)
# window_h = screen_h * vector
# window_w = screen_w * vector
#
# if h > window_h or w > window_w:
#     if h / window_h >= w / window_w:
#         multiplier = window_h / h
#     else:
#         multiplier = window_w / w
#     img = cv2.resize(img, (0, 0), fx=multiplier, fy=multiplier)

cv2.imshow('image_deirvlon', img)
# print(decoded.shape)
cv2.waitKey(5_000)
cv2.destroyAllWindows()
