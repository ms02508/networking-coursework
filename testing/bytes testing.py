import hashlib
from caesarcipher import CaesarCipher

message = "Did you ever hear the tragedy of Darth Plagueis The Wise? I thought not. It’s not a story the Jedi would tell you. It’s a Sith legend. Darth Plagueis was a Dark Lord of the Sith, so powerful and so wise he could use the Force to influence the midichlorians to create life… He had such a knowledge of the dark side that he could even keep the ones he cared about from dying. The dark side of the Force is a pathway to many abilities some consider to be unnatural. He became so powerful… the only thing he was afraid of was losing his power, which eventually, of course, he did. Unfortunately, he taught his apprentice everything he knew, then his apprentice killed him in his sleep. Ironic. He could save others from death, but not himself."
b_message = bytes(message, 'utf-8')


# def cipher_encrypt(b_msg, numPackets):
#     plain_text = b_msg.decode("utf-8")
#     encrypted = ""
#     key = (69 - numPackets) * 2
#     for c in plain_text:
#         if c.isupper():  # check if it's an uppercase character
#             c_index = ord(c) - ord('A')
#             # shift the current character by key positions
#             c_shifted = (c_index + key) % 26 + ord('A')
#             c_new = chr(c_shifted)
#             encrypted += c_new
#         elif c.islower():  # check if its a lowecase character
#             # subtract the unicode of 'a' to get index in [0-25) range
#             c_index = ord(c) - ord('a')
#             c_shifted = (c_index + key) % 26 + ord('a')
#             c_new = chr(c_shifted)
#             encrypted += c_new
#         elif c.isdigit():
#             # if it's a number,shift its actual value
#             c_new = (int(c) + key) % 10
#             encrypted += str(c_new)
#         else:
#             # if its neither alphabetical nor a number, just leave it like that
#             encrypted += c
#     return bytes(encrypted, "utf-8")
#
#
# The Decryption Function
# def cipher_decrypt(b_msg, numPackets):
#     key = (69 - numPackets) * 2
#     ciphertext = b_msg.decode("utf-8")
#     decrypted = ""
#     for c in ciphertext:
#         if c.isupper():
#             c_index = ord(c) - ord('A')
#             # shift the current character to left by key positions to get its original position
#             c_og_pos = (c_index - key) % 26 + ord('A')
#             c_og = chr(c_og_pos)
#             decrypted += c_og
#         elif c.islower():
#             c_index = ord(c) - ord('a')
#             c_og_pos = (c_index - key) % 26 + ord('a')
#             c_og = chr(c_og_pos)
#             decrypted += c_og
#         elif c.isdigit():
#             # if it's a number,shift its actual value
#             c_og = (int(c) - key) % 10
#             decrypted += str(c_og)
#         else:
#             # if its neither alphabetical nor a number, just leave it like that
#             decrypted += c
#     return bytes(decrypted, "utf-8")
#
#
# e_msg = cipher_encrypt(b_message, 5)
#
# print(e_msg.decode("utf-8"))
# print(cipher_decrypt(e_msg, 5).decode("utf-8"))
# print(message)
# b_message = bytes(message, 'utf-8')
# checksum = hashlib.md5(b_message)
# print(checksum.digest())
# print(len(checksum.digest()))
# print(message)
#
# print(len(b_message))
# checksum = 0
# for byte in b_message:
#     print(byte, end=" ")
#     checksum += byte
# print(b_message)
# print()
# print(checksum)

# return_packets = []
# msg_parts = []
# len_count = 0
#
# # split up teh message into chunks of 250
# while len_count < len(b_message):
#     msg_parts.append(b_message[len_count: (len_count + 250)])
#     len_count += 250
#
# for i in range(len(msg_parts)):
#     return_packets.append(
#         [msg_parts[i], i+1, len(msg_parts)]
#     )
#
# for pack in return_packets:
#     print(pack)
