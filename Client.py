# Matthew Samm
# COM2022 - Computer Networking

# imports
import time
from socket import *

import cv2
import numpy as np

from ImageCompiler import ImageCompiler
from Server import Server
from RecMsg import RecMsg

# Use multithreading to launch other commands while the server is still listening for messages?


class Client:
    """class from which everything is controlled, protocol is p2p so everything is obfuscated behind the main client"""

    def __init__(self, portNum, doEncrypt=False):
        # client socket handles sending messages
        self.clientSocket = socket(AF_INET, SOCK_DGRAM)
        # internal server is constantly listening for messages
        self.internalServer = Server(portNum)
        self.buffer = 256
        self.doEncrypt = doEncrypt

    def setEncrypt(self, doEncrypt):
        self.doEncrypt = doEncrypt

    def getEncrypt(self):
        return self.doEncrypt

    def getPortNum(self):
        return self.internalServer.getPortNum()

    def changeLocalPort(self, newPort):
        self.internalServer.changePort(newPort)

    def openReceiveConnection(self):
        """Set the internal server to start listening for messages"""
        self.internalServer.openCon()

    def closeRecConnection(self):
        self.internalServer.closeCon()

    def sendMessage(self, message, targetIP, targetPortNum):
        """Send a message to a specified IP/port"""
        # Testing code to make sure messages can be sent
        print("in send msg")
        print("msg =\n" + message)
        print()
        # self.clientSocket.sendto(bytes(message, "utf-8"), (targetIP, targetPortNum))

        # First do handshake
        if self.handshaking(targetIP, targetPortNum):
            print("can send message")
            # check if message needs to be split
            packets = self.partitionMsg(message)
            print("got packets")

            # send every packet that's in list of packets for message
            for packet in packets:
                print(len(packet))
                print(packet)
                self.clientSocket.sendto(packet, (targetIP, targetPortNum))

            print("sent packets")

            # wait for ack of all messages received / handle re-sending packets
            self.clientSocket.settimeout(1)
            try:
                data, address = self.clientSocket.recvfrom(256)

            except timeout as inst:
                print("rec ACK timout, retransmit")

            else:
                rec_msg = RecMsg(data, self.doEncrypt)
                if rec_msg.testForAck():
                    print("rec ACK, all messages ok")

                elif rec_msg.testForResend():
                    # resend packets
                    print("re-send request received")

                    # received a request to re-send, so respond with ACK
                    self.internalServer.sendAck(address)

                    # needs to listen out for packet

                    try:
                        data, address = self.clientSocket.recvfrom(256)
                        # deal with requested packets here

                        # split the packet data into a list
                        rec_msg = RecMsg(data, self.doEncrypt)
                        msg_data = rec_msg.getData().decode("utf-8")
                        req_packs = msg_data.split(",")
                        print(req_packs)
                        repeat_pack_list = []

                        # get the packets that correspond to the given IDs
                        for ID in req_packs:
                            print(ID)
                            repeat_pack_list.append(packets[int(ID)])

                        # re-send packets
                        for p in repeat_pack_list:
                            self.clientSocket.sendto(p, address)
                    except timeout as inst:
                        print("rec request timout")

                    else:
                        pass

    def sendImage(self, imageLocation, targetIP, targetPortNum):
        print("in send image")
        if self.handshaking(targetIP, targetPortNum):
            print("can send message")

            image = cv2.imread(imageLocation, 1)

            if self.sendImgData(image, targetIP, targetPortNum):
                packets = self.partitionImg(image)

                print("sending image packets")
                for pack in packets:
                    next_p = False
                    while not next_p:
                        # send packet
                        self.clientSocket.sendto(pack, (targetIP, targetPortNum))
                        rec = None
                        # wait for ack
                        while rec == None:
                            rec, address = self.clientSocket.recvfrom(256)
                            rec_msg = RecMsg(rec, False)
                            if rec_msg.testForAck():
                                next_p = True
                    # time.sleep(1/10_000_000)
                    # a = 10240 ** 20088

            # create img data packet

    def handshaking(self, ip, port):
        """method that controls the handshaking protocol. Returns true if response heard within 5 seconds"""
        send_ok = False

        # the request message
        s_msg = "011000100111010101101110011001000110000100001010"
        b_msg = bytes("011000100111010101101110011001000110000100001010", "utf-8")
        print("msg = ", b_msg)
        # print(len(b_msg))

        # create a packet with the auth msg, it will have 1 packet
        pack = self.createPacket(b_msg, 0, 1)
        # print(pack)
        # print(pack.hex())
        # print(str(pack))
        print("made packet")

        # set timeout on the socket
        self.clientSocket.settimeout(1)

        # send the packet
        for i in range(1, 5):
            # try 5 times to get an acknowledgement
            try:
                # send request packet
                print("try sending msg")
                self.clientSocket.sendto(pack, (ip, port))

                print("waiting for response")
                data, address = self.clientSocket.recvfrom(256)
                rec_msg = RecMsg(data, self.doEncrypt)
                # print("received pack in handshake:\n", data, "\n")
                if rec_msg.testForAck():
                    pass
                else:
                    raise timeout
                print("data = ", data)
                print("decoded data = ", str(data))

            except timeout as inst:
                print("Request timout")
                if i < 5:
                    print("Trying again")
                else:
                    print("Failed to establish a connection")

            else:
                send_ok = True
                break

        return send_ok

    def sendImgData(self, image, targetIP, targetPort):
        send_ok = False
        h, w, c = image.shape
        enc_h = h.to_bytes(2, "big")
        enc_w = w.to_bytes(2, "big")
        enc_c = c.to_bytes(2, "big")

        data_msg = b"image_data"
        # pack_data = data_msg + enc_w + enc_h + enc_c
        pack_data = data_msg + enc_h + enc_w + enc_c

        checksum = self.createChecksum(pack_data)
        header = self.createImgHeader(1, 0, checksum)
        packet = header + pack_data

        for i in range(1, 5):
            try:
                print("try sending msg")
                self.clientSocket.sendto(packet, (targetIP, targetPort))

                data, address = self.clientSocket.recvfrom(256)
                rec_msg = RecMsg(data, self.doEncrypt)
                # print("received pack in handshake:\n", data, "\n")
                if rec_msg.testForAck():
                    pass
                else:
                    raise timeout

            except timeout as inst:
                print("message data timeout")
                if i < 5:
                    print("Trying again")
                else:
                    print("Failed to establish a connection")

            else:
                print("send rest of image packets")
                send_ok = True
                break

        return send_ok

    def partitionImg(self, image):
        """take an image and splits it across packets to be sent"""
        return_packets = []
        print("start img partition")

        # get image data - height, width, channels
        # and convert image data to bytes
        h, w, c = image.shape
        # resize inserted image
        data = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        data = cv2.resize(data, (w, h))

        # run a color convert:
        data = cv2.cvtColor(data, cv2.COLOR_BGR2RGB)
        b_image = bytes(data)  # encode Numpy to Bytes string
        # print(b_image)

        if len(b_image) <= 246:
            # don't need to split message
            return_packets.append(
                self.createImgPacket(b_image, 0, 1)
            )

        else:
            # image needs to be split
            msg_parts = []
            len_count = 0

            # split image byte string
            while len_count < len(b_image):
                msg_parts.append(b_image[len_count: (len_count + 246)])
                len_count += 246
                if len_count + 246 > len(b_image):
                    # print("£"*10)
                    # print("adding remaining partitions")
                    # print("£" * 10)
                    msg_parts.append(b_image[len_count:])
                    break

            # create packets for partitioned image
            for i in range(len(msg_parts)):
                return_packets.append(
                    self.createImgPacket(msg_parts[i], i, len(msg_parts))
                )

        # for pack in return_packets:
        #     print(pack)

        return return_packets

    def partitionMsg(self, msg):
        """takes a message and splits it into chunks of the buffer size - header size.
        Returns a list of the chunks"""
        return_packets = []
        print("start partition")

        # check if message needs to be split
        b_msg = msg.encode("utf-8")
        if len(b_msg) <= 246:
            # don't need to split message
            return_packets.append(
                self.createPacket(b_msg, 0, 1)
            )

        else:
            # message needs to be split
            msg_parts = []
            len_count = 0

            # split up teh message into chunks of 250
            # print("enter len loop")

            while len_count < len(b_msg):
                # split the message on spaces to avoid encoding issues
                found_space = False
                count = 246

                # todo: escape when end of string
                while not found_space:
                    # print(count)
                    # print(msg[(len_count + (count - 1))])
                    if len_count + count > len(b_msg):
                        msg_parts.append(b_msg[len_count:])
                        found_space = True
                        len_count += count
                    else:
                        if msg[(len_count + (count - 1))] == " ":
                            msg_parts.append(b_msg[len_count: (len_count + count)])
                            # print("found space")
                            len_count += count
                            found_space = True

                        elif len_count + count == len(b_msg):
                            msg_parts.append(b_msg[len_count: (len_count + count)])
                            len_count += count
                            found_space = True
                        else:
                            count -= 1

            # for part in msg_parts:
            #     print(part)

            # print("Exit len loop")

            # for i in msg_parts:
            #     print(len(i))

            # for every entry in msg_parts, create a packet out of the message section
            # and append to the list of packets
            # packet ID starts at 1
            # print("packet creation loop start")
            print(len(msg_parts))

            for i in range(len(msg_parts)):
                return_packets.append(
                    self.createPacket(msg_parts[i], i, len(msg_parts))
                )
        # print("partitioning finished")
        return return_packets

    def createImgPacket(self, img, packId, totalPacks):
        checksum = self.createChecksum(img)
        header = self.createImgHeader(packId, totalPacks, checksum)

        packet = header + img
        return packet

    def createPacket(self, msg, packId, total):
        """creates a whole packet out of a given message (adds header and its values).
        Enter the msg as bytes"""
        # packet = b""
        print("create packet")

        checksum = self.createChecksum(msg)
        header = self.createHeader(packId, total, checksum)

        print("="*20)
        print("b_msg = ", msg)
        print("decoded = ", msg.decode("utf-8"))
        print("=" * 20)

        if self.doEncrypt:
            enc_msg = self.cipher_encrypt(msg, total)
            packet = header + enc_msg
        else:
            packet = header + msg

        return packet

    def createImgHeader(self, packId, totalPacks, checksum):
        encoded_id = packId.to_bytes(2, "big")
        encoded_total = totalPacks.to_bytes(2, "big")
        encoded_check = checksum.to_bytes(2, "big")

        img_data = b"_img"

        header = encoded_id + encoded_total + encoded_check + img_data

        return header

    def createHeader(self, packID, total, check):
        """creates a header from given data. Returns the total header."""
        print("create header")
        # First 2 bytes are for the packet ID
        # Second 2 bytes are for the total number of packets
        # Third 2 bytes are for the checksum

        # encode numbers
        encoded_id = packID.to_bytes(2, "big")
        encoded_total = total.to_bytes(2, "big")
        encoded_check = check.to_bytes(2, "big")
        encoded_blank = (0).to_bytes(4, "big")
        b_header = encoded_id + encoded_total + encoded_check + encoded_blank
        # t_header = encoded_id + encoded_total + encoded_check
        # print(b_header)
        # print(t_header)
        # add them to header

        # s_header = str(packID).zfill(2) + str(total).zfill(2) + str(check)

        # print("header = ", b_header)
        # print(len(b_header))
        return b_header

    def createChecksum(self, msg):
        """Creates a checksum from the given data. Returns the 2 byte representation"""
        # print("create checksum")
        check = len(msg)
        # validate checksum in range 1-99
        valid_check = False
        check = check % 100

        # s_check = str(check).zfill(2)
        return check

    def cipher_encrypt(self, b_msg, numPackets):
        plain_text = b_msg.decode("utf-8")
        encrypted = ""
        key = (69 - numPackets) * 2
        for c in plain_text:
            if c.isupper():  # check if it's an uppercase character
                c_index = ord(c) - ord('A')
                c_shifted = (c_index + key) % 26 + ord('A')
                c_new = chr(c_shifted)
                encrypted += c_new
            elif c.islower():  # check if its a lowercase character
                c_index = ord(c) - ord('a')
                c_shifted = (c_index + key) % 26 + ord('a')
                c_new = chr(c_shifted)
                encrypted += c_new
            elif c.isdigit():
                # if it's a number,shift its actual value
                c_new = (int(c) + key) % 10
                encrypted += str(c_new)
            else:
                encrypted += c

        return bytes(encrypted, "utf-8")
