# UDPPingerClient.py
# We will need the following module to generate randomized lost packets
import sys
from socket import *
import time as t
import datetime

# Create a UDP socket
UDP_IP_ADDRESS = "127.0.0.1"
UDP_PORT_NO = 12000
print("hi")
# Message = bytes("Hello world", "utf-8")
plain_num = 1024
encoded_int = plain_num.to_bytes(2, "big")
Message = encoded_int
print(len(Message))


# create a socket with a 1s timeout.
clientSock = socket(AF_INET, SOCK_DGRAM)
clientSock.settimeout(1)

for i in range(0, 10):
	# Send data
	print('sending "%s"' % Message)

	try:
		# sent the Message using the clientSock
		startT = t.time_ns()
		a = datetime.datetime.now()
		clientSock.sendto(Message, (UDP_IP_ADDRESS, UDP_PORT_NO))
		# sent=
		# Receive response
		print('waiting to receive')

		data, add = clientSock.recvfrom(1024)

		print('received "%s"' % data)
		b = datetime.datetime.now()
		print("took %d ns" % (t.time_ns() - startT))
		c = b-a
		print("a", a)
		print("b", b)
	except timeout as inst:
		# handle timeouts
		print("Request timeout")

print('closing socket')
clientSock.close()
