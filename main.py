# Matthew Samm
# COM2022 - Computer Networking

# imports
from Client import Client
import multiprocessing

# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    # variable initialisation

    running = True
    UDP_IP_ADDRESS = "127.0.0.1"
    UDP_PORT_NO = 12000

    # create the client instance
    localPortNum = int(input("Enter default local port: "))
    asdf = Client(localPortNum)

    # this code starts the while loop that listens for a packet
    listen_proc = multiprocessing.Process(target=asdf.openReceiveConnection)
    listen_proc.start()

    while running:
        user_in = input("""
                Welcome.
        What would you like to do:
        1) send message
        2) set destination address
        3) set local port number
        4) set encryption status
        5) send image
        6) close
        
        """)

        if user_in == "1":
            user_msg = input("""
            Enter your message:
            """)

            asdf.sendMessage(user_msg, UDP_IP_ADDRESS, UDP_PORT_NO)

        elif user_in == "2":
            print("the current address is: (" + str(UDP_IP_ADDRESS) + ", " + str(UDP_PORT_NO) + ")")

            print("""
            Which value do you want to change:
            1) IP Address
            2) Port Number
            3) Both
            4) None
            """)
            user_add_in = input()

            if user_add_in == "1":
                print("Enter the new IP address")
                new_IP = input()
                UDP_IP_ADDRESS = new_IP

            elif user_add_in == "2":
                print("Enter the new port number")
                try:
                    new_port_add = int(input())
                    UDP_PORT_NO = new_port_add
                except ValueError:
                    print("enter a number")

            elif user_add_in == "3":
                print("Enter the new IP address")
                new_IP = input()
                UDP_IP_ADDRESS = new_IP

                print("Enter the new port number")
                try:
                    new_IP = int(input())

                except ValueError:
                    print("enter a number")

            elif user_add_in == "4":
                pass

            else:
                print("incorrect input")

        elif user_in == "3":
            print("The current port number is: " + str(asdf.getPortNum()))
            print("would you like to change it? y/n")
            user_port_conf = input()
            if user_port_conf.lower() == "y":
                # change port
                try:
                    newPort = int(input("Enter the new port:\n\t"))
                    # change the port on the client's internal server
                    asdf.changeLocalPort(newPort)

                    # terminate the thread that holds the current internal server
                    listen_proc.terminate()
                    listen_proc.join()

                    # launch a new Process with the new socket
                    listen_proc = multiprocessing.Process(target=asdf.openReceiveConnection)
                    listen_proc.start()

                except ValueError as e:
                    print("Incorrect Input\n")

        elif user_in == "4":
            print("Current encryption status:")
            print("SEND ENCRYPTION =", asdf.getEncrypt())
            print("RECEIVE DECRYPTION =", asdf.internalServer.getDecrypt())

            user_enc = input("""
            Which value do you wish to change
            1) SEND
            2) RECEIVE
            3) BOTH
            4) NONE
            """)

            if user_enc == "1":
                asdf.setEncrypt(not asdf.getEncrypt())
                print("changed SEND to", asdf.getEncrypt())

            elif user_enc == "2":
                asdf.internalServer.setDecrypt(not asdf.internalServer.getDecrypt())
                print("changed RECEIVE to", asdf.internalServer.getDecrypt())

                listen_proc.terminate()
                listen_proc.join()

                # launch a new Process with the new socket
                listen_proc = multiprocessing.Process(target=asdf.openReceiveConnection)
                listen_proc.start()

            elif user_enc == "3":
                asdf.setEncrypt(not asdf.getEncrypt())
                asdf.internalServer.setDecrypt(not asdf.internalServer.getDecrypt())

                print("changed SEND to", asdf.getEncrypt())
                print("changed RECEIVE to", asdf.internalServer.getDecrypt())

                listen_proc.terminate()
                listen_proc.join()

                # launch a new Process with the new socket
                listen_proc = multiprocessing.Process(target=asdf.openReceiveConnection)
                listen_proc.start()

        elif user_in == "5":
            img_loc = input("""
            enter path of image:
            """)

            asdf.sendImage(img_loc, UDP_IP_ADDRESS, UDP_PORT_NO)

        elif user_in == "6":
            running = False

        else:
            print("\nIncorrect Input\n")

    # test code
    print("asdf")

    # gracefully close the internal server process
    listen_proc.terminate()
    listen_proc.join()

    # closeCon will close the socket
    asdf.internalServer.closeCon()
